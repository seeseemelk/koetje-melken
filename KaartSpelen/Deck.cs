﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaartSpelen
{
    public class Deck
    {
        private List<Card> cards = new List<Card>();

        public void AddCard(Card card)
        {
            cards.Add(card);
        }

        public Card TakeCard()
        {
            Card card = cards[cards.Count - 1];
            cards.RemoveAt(cards.Count - 1);
            return card;
        }

        public void Clear()
        {
            cards.Clear();
        }

        public void Fill(string[] values, string[] suits)
        {
            for(int i = 0; i < values.Length; i++)
            {
                foreach(string suit in suits)
                {
                    Card card = new Card(values[i], suit, i);
                    AddCard(card);
                }
            }
        }

        public void Shuffle()
        {
            Random shuffle = new Random(Guid.NewGuid().GetHashCode());

            List<Card> newCards = new List<Card>();
            while(cards.Count > 0)
            {
                int i = shuffle.Next(cards.Count);
                newCards.Add(cards[i]);
                cards.RemoveAt(i);
            }
            cards = newCards;
        }

        public Card GetTopCard()
        {
            return cards[cards.Count - 1];
        }

        public Card GetCard(int i)
        {
            return cards[i];
        }

        public Boolean IsEmpty()
        {
            if(cards.Count == 0)
            {
                return true;
            }
            return false;
        }

        public int Count()
        {
            return cards.Count;
        }

        public void Sort()
        {
            for (int c = 1; c <= cards.Count - 1; c++)
            {
                for (int i = 0; i < cards.Count - c; i++)
                {
                    if (cards[i].Score < cards[i+1].Score)
                    {
                        Card t = cards[i];
                        cards[i] = cards[i + 1];
                        cards[i + 1] = t;
                    }
                }
            }
        }

    }
}
