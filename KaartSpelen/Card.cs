﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace KaartSpelen
{
    public class Card
    {
        public string Value { get; set; }
        public string Suit { get; set; }
        public int Score { get; set; }

        public Card(string value, string suit, int score)
        {
            Value = value;
            Suit = suit;
            Score = score;
        }

        public Card(string value, string suit)
        {
            Value = value;
            Suit = suit;
        }

        public override string ToString()
        {
            return Value + " of " + Suit;
        }

        public BitmapImage GetImage()
        {
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.UriSource = new Uri($"resources/{Value}{Suit}.png", UriKind.RelativeOrAbsolute);
            bi.EndInit();
            return bi;
        }
    }
}
