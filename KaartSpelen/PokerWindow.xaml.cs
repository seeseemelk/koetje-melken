﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace KaartSpelen
{
    /// <summary>
    /// Interaction logic for Poker.xaml
    /// </summary>
    public partial class PokerWindow : Window
    {
		private enum WinTypes
		{
			HIGH_CARD, PAIR, TWO_PAIRS, THREE_OF_A_KIND, STRAIGHT, FLUSH,
			FULL_HOUSE, FOUR_OF_A_KIND, STRAIGHT_FLUSH
		}

        private string[] values;
        private string[] suits;
        private Deck mainDeck = new Deck();
        private Deck computerTable = new Deck();
        private Deck playerTable = new Deck();
        private DispatcherTimer timer = new DispatcherTimer();
        private Canvas[] computerCards;
        private Canvas[] playerCards;
		private int playerWins;
		private int computerWins;

        public PokerWindow(string[] values, string[] suits)
        {
            InitializeComponent();
            this.values = values;
            this.suits = suits;
            timer.Interval = TimeSpan.FromSeconds(2);
            timer.Tick += Timer_Tick;
            computerCards = new Canvas[] {card1Computer, card2Computer, card3Computer, card4Computer, card5Computer};
            playerCards = new Canvas[] { card1Player, card2Player, card3Player, card4Player, card5Player };
        }

		private static string GetWinType(WinTypes type)
		{
			switch (type)
			{
				case WinTypes.HIGH_CARD: return "High Card";
				case WinTypes.PAIR: return "Pair";
				case WinTypes.TWO_PAIRS: return "Two Pairs";
				case WinTypes.THREE_OF_A_KIND: return "Three Of A Kind";
				case WinTypes.STRAIGHT: return "Straight";
				case WinTypes.FLUSH: return "Flush";
				case WinTypes.FULL_HOUSE: return "Full House";
				case WinTypes.FOUR_OF_A_KIND: return "Four Of A Kind";
				case WinTypes.STRAIGHT_FLUSH: return "Straight Flush";
				default: return type.ToString();
			}
		}

		private int GetHighestCount(int[] cards)
		{
			int highestIndex = 0;
			foreach (int i in cards)
			{
				if (i >= cards[highestIndex])
					highestIndex = i;
			}
			return highestIndex;
		}

		private int GetMaxContinuous(int[] cards)
		{
			int max = 0;
			int continuous = 0;
			foreach (int i in cards)
			{
				if (i == 0)
					continuous = 0;

				continuous++;
				max = Math.Max(max, continuous);
			}
			return max;
		}

		private WinTypes CalculateWinType(Deck deck, out int rank, out int[] cardCount)
		{
			bool allSameSuit = true;
			string suit = deck.GetTopCard().Suit;
			cardCount = new int[values.Length+1];

			for (int i = 0; i < deck.Count(); i++)
			{
				Card card = deck.GetCard(i);
				cardCount[card.Score]++;
				if (card.Score == 0) // Ace is both lowest and highest card
					cardCount[values.Length - 1]++;

				if (card.Suit != suit) // We also check if all cards are the same suit her
					allSameSuit = false;
			}

			// Contains the largest string of continuous cards.
			// E.g: [0,0,1,1,1,0,1] will return 3.
			int continuous = GetMaxContinuous(cardCount);
	
			// Gets the number of cards that the highest card has.
			// This means if you have 3 cards of the same value,
			// highestCount will also be 3.
			// E.g: [0,0,1,0,3,1] will return 3.
			int highestCountIndex = GetHighestCount(cardCount);
			int highestCount = cardCount[highestCountIndex];
			rank = highestCountIndex;

			// Check for a Straight Flush
			if (allSameSuit && continuous == 5)
			{
				return WinTypes.STRAIGHT_FLUSH;
			}

			// Check for Four Of A Kind
			if (highestCount == 4)
				return WinTypes.FOUR_OF_A_KIND;

			// Check for Full House
			bool hasThreeOfAKind = (highestCount == 3);
			bool hasTwoOfAKind = false;
			foreach (int i in cardCount)
			{
				if (i == 2)
				{
					hasTwoOfAKind = true;
					break;
				}
			}

			if (hasThreeOfAKind && hasTwoOfAKind)
				return WinTypes.FULL_HOUSE;

			// Check for Flush
			if (allSameSuit)
				return WinTypes.FLUSH;

			// Check for Straight
			if (continuous == 5)
				return WinTypes.STRAIGHT;

			// Check for Three Of A Kind
			if (hasThreeOfAKind)
				return WinTypes.THREE_OF_A_KIND;

			// Check for two Pairs
			int numPairs = 0;
			foreach (int i in cardCount)
			{
				if (i == 2)
					numPairs++;
			}

			if (numPairs == 2)
				return WinTypes.TWO_PAIRS;

			// Check for a Pair
			if (numPairs == 1)
				return WinTypes.PAIR;

			// Else, we just have a high card
			return WinTypes.HIGH_CARD;
		}

		private WinTypes CalculateWinType(Deck deck)
		{
			int rankComputer;
			int[] cardsComputer;
			return CalculateWinType(deck, out rankComputer, out cardsComputer);
		}

		private bool DidPlayerWin()
		{
			int[] cardsComputer;
			int rankComputer;
			WinTypes winComputer = CalculateWinType(computerTable, out rankComputer, out cardsComputer);
			int[] cardsPlayer;
			int rankPlayer;
			WinTypes winPlayer = CalculateWinType(playerTable, out rankPlayer, out cardsPlayer);

			if (winPlayer > winComputer)
				return true;
			else if (winPlayer < winComputer)
				return false;

			if (rankPlayer > rankComputer)
				return true;
			else if (rankPlayer < rankComputer)
				return false;

			for (int i = cardsComputer.Length - 1; i >= 0; i--)
			{
				if (cardsPlayer[i] > cardsComputer[i])
					return true;
				else if (cardsPlayer[i] < cardsComputer[i])
					return false;
			}

			// Both computer and player have the exact same cards (except for suit)
			return true;
		}

        private void Timer_Tick(object sender, EventArgs e)
        {
			if (computerTable.Count() > 0)
			{
				// We need to calculate the winner.
				bool didPlayerWin = DidPlayerWin();
				if (didPlayerWin)
				{
					statusTextBlock.Text = "You won";
					playerWins++;
				}
				else
				{
					statusTextBlock.Text = "The computer won";
					computerWins++;
				}
			}

            timer.IsEnabled = false;
            shuffledLabel.Visibility = Visibility.Hidden;
            computerTable.Clear();
            playerTable.Clear();
            dealButton.IsEnabled = true;
            if (mainDeck.Count() >= 5)
            {
                for(int i = 0; i < 5; i++)
                {
                    Card computerCard = mainDeck.TakeCard();
                    computerTable.AddCard(computerCard);
                    
                }
                computerTable.Sort();
                for(int i = 0; i < 5; i++)
                {
                    Draw(i);
                }
            }

			if (computerTable.Count() >= 5)
				computerWinTypeLabel.Content = GetWinType(CalculateWinType(computerTable));
			else
				computerWinTypeLabel.Content = "";

			playerWinTypeLabel.Content = "";
        }

        private void Draw(Canvas canvas, Card card)
        {
            canvas.Children.Clear();
            Image cardImage = new Image();
            cardImage.Source = card.GetImage();
            cardImage.Width = canvas.Width;
            cardImage.Height = canvas.Height;
            cardImage.MouseDown += dealButton_Click;
            canvas.Children.Add(cardImage);
        }

        private void Draw(int canvasNumber)
        {
            Draw(computerCards[canvasNumber], computerTable.GetCard(canvasNumber));
            if (canvasNumber < playerTable.Count())
            {
                Draw(playerCards[canvasNumber], playerTable.GetCard(canvasNumber));
            } else
            {
                playerCards[canvasNumber].Children.Clear();
            }
        }

        private void shuffleButton_Click(object sender, RoutedEventArgs e)
        {
            dealButton.IsEnabled = false;
            computerTable.Clear();
            playerTable.Clear();
            mainDeck.Clear();

            mainDeck.Fill(values, suits);
            mainDeck.Shuffle();

			playerWins = 0;
			computerWins = 0;

            shuffledLabel.Visibility = Visibility.Visible;
            timer.IsEnabled = true;
            
        }

        private void dealButton_Click(object sender, RoutedEventArgs e)
        {
            if (mainDeck.Count() >= 5)
            {
                for (int i = 0; i < 5; i++)
                {
                    Card playerCard = mainDeck.TakeCard();
                    playerTable.AddCard(playerCard);
                }

                playerTable.Sort();

                for(int i = 0; i < 5; i++)
                {
                    Draw(i);
                }

                timer.IsEnabled = true;
                dealButton.IsEnabled = false;
				playerWinTypeLabel.Content = GetWinType(CalculateWinType(playerTable));
            }
			else
			{
				computerWinTypeLabel.Content = "NO MORE CARDS TO DEAL";
				playerWinTypeLabel.Content = computerWinTypeLabel.Content;

				if (playerWins > computerWins)
				{
					statusTextBlock.Text = $"Congratulations!! You are the winner!! " +
						$"The computer won {computerWins} times, you won {playerWins} times!";
				}
				else if (playerWins < computerWins)
				{
					statusTextBlock.Text = $"Sorry, you lost. The computer won {computerWins} times, " +
						$"you won {playerWins} times.";
				}
				else
				{
					statusTextBlock.Text = $"No winner, you have the same result as the computer: " +
						$"both won {playerWins} times.";
				}
			}
        }
    }
}
