﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KaartSpelen
{
    /// <summary>
    /// Interaction logic for HogerLagerWindow.xaml
    /// </summary>
    public partial class HogerLagerWindow : Window
    {
        private string[] values;
        private string[] suits;
        private Card card;

        public HogerLagerWindow(string[] values, string[] suits)
        {
            InitializeComponent();
            this.values = values;
            this.suits = suits;
            suitsComoBox.SelectionChanged += suitsComoBox_SelectionChanged;
            valueSlider.ValueChanged += valueSlider_ValueChanged;
        }

        public void Draw(Canvas canvas)
        {
            canvas.Children.Clear();
            string suit = suitsComoBox.SelectedValue.ToString();
            string value = values[(int)valueSlider.Value];
            card = new Card(value, suit);
            Image cardImage = new Image
            {
                Source = card.GetImage(),
                Width = canvas.Width
            };
            canvas.Children.Add(cardImage);
        }

        private void suitsComoBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Draw(cardCanvas);
        }

        private void valueSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Draw(cardCanvas);
        }

        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            int aantalKaarten = Convert.ToInt32(aantalTextBox.Text);
            if (aantalKaarten > 50 || aantalKaarten < 2) 
            {
                MessageBox.Show("Invalid number");
            } else
            {
                GameWindow window = new GameWindow(values, suits, card, aantalKaarten);
                window.Visibility = Visibility.Visible;
            }
        }
    }
}
