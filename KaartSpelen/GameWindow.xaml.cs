﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KaartSpelen
{
    /// <summary>
    /// Interaction logic for GameWindow.xaml
    /// </summary>
    public partial class GameWindow : Window
    {
        private string[] values;
        private string[] suits;
        private int aantal;
        private Card mainCard;
        private Card nextCard;
        private int count = 0;
        private List<Card> cards = new List<Card>();
        private List<Canvas> canvasses = new List<Canvas>();
        private BitmapImage cardback;
        private Deck mainDeck = new Deck();


        public GameWindow(string[] values, string[] suits, Card firstCard, int aantal)
        {
            InitializeComponent();
            this.aantal = aantal;
            this.values = values;
            this.suits = suits;
            mainCard = firstCard;
            cardback = new BitmapImage();
            cardback.BeginInit();
            cardback.UriSource = new Uri("resources/cardback.png", UriKind.RelativeOrAbsolute);
            cardback.EndInit();
            double gap = (drawCanvas.Width - 100) / (aantal - 1);
            double x = 0;
            for(int i = 0; i < aantal; i++)
            {
                Canvas canvas = new Canvas();
                canvas.Width = 100;
                canvas.Height = 150;
                canvas.Margin = new Thickness(x, 20, 0, 0);
                canvas.HorizontalAlignment = HorizontalAlignment.Left;
                canvas.VerticalAlignment = VerticalAlignment.Top;
                canvas.Background = new SolidColorBrush(Colors.LightGray);
                drawCanvas.Children.Add(canvas);
                x += gap;
                canvasses.Add(canvas);
            }
            Draw(canvasses[0], firstCard);
            mainDeck.Fill(values, suits);
            mainDeck.Shuffle();
            for(int i = 1; i < aantal; i++)
            {
                Card card = mainDeck.TakeCard();
                cards.Add(card);
                DrawReverse(canvasses[i], card);
            }
        }

        private void Draw(Canvas canvas, Card card)
        {
            canvas.Children.Clear();
            Image cardImage = new Image();
            cardImage.Source = card.GetImage();
            cardImage.Width = canvas.Width;
            cardImage.Height = canvas.Height;
            canvas.Children.Add(cardImage);
        }

        private void DrawReverse(Canvas canvas, Card card)
        {
            canvas.Children.Clear();
            Image cardImage = new Image();
            cardImage.Source = cardback;
            cardImage.Width = canvas.Width;
            cardImage.Height = canvas.Height;
            canvas.Children.Add(cardImage);
            }

        private void higherButton_Click(object sender, RoutedEventArgs e)
        {
            if(count < aantal - 1)
            {
                nextCard = cards[count];
                if (nextCard.Score >= mainCard.Score)
                {
                    Draw(canvasses[count + 1], nextCard);
                    mainCard = cards[count];
                    count++;
                    if (count >= aantal - 1)
                    {
                        MessageBox.Show("Proficiat, u heeft gewonnen");
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Jammer, volgende keer beter");
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Proficiat, u heeft gewonnen");
                this.Close();
            }
            

        }

        private void lowerButton_Click(object sender, RoutedEventArgs e)
        {
            if(count < aantal - 1)
            {
                nextCard = cards[count];
                if (nextCard.Score <= mainCard.Score)
                {
                    Draw(canvasses[count + 1], nextCard);
                    mainCard = cards[count];
                    count++;
                    if(count >= aantal - 1)
                    {
                        MessageBox.Show("Proficiat, u heeft gewonnen");
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Jammer, volgende keer beter");
                    this.Close();
                }
            }
           
        }

    }
    }

