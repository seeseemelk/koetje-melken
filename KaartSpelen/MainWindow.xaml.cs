﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KaartSpelen
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string[] values;
        private string[] suits;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void openMenuItem_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text documents|*.txt|All files|*.*";

            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    StreamReader inputStream = File.OpenText(openFileDialog.FileName);
                    string line = inputStream.ReadLine();
                    values = line.Split(',');
                    for (int i = 0; i < values.Length; i++)
                    {
                        values[i] = values[i].Trim();
                    }
                    line = inputStream.ReadLine();
                    suits = line.Split(',');
                    for (int i = 0; i < suits.Length; i++)
                    {
                        suits[i] = suits[i].Trim();
                    }

                    koetjeMelkenRadioButton.IsEnabled = true;
                    hogerLagerRadioButton.IsEnabled = true;
                    pokerRadioButton.IsEnabled = true;
                    startButton.IsEnabled = true;

                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error concerning file: " + openFileDialog.FileName + ": " + ex.Message);
                }
            }
        }

        private void exitMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            if (koetjeMelkenRadioButton.IsChecked == true)
            {
                KoetjeMelkenWindow window = new KoetjeMelkenWindow(values, suits);
                window.Visibility = Visibility.Visible;
            } else if(hogerLagerRadioButton.IsChecked == true)
            {
                HogerLagerWindow window = new HogerLagerWindow(values, suits);
                window.Visibility = Visibility.Visible;
            } else if(pokerRadioButton.IsChecked == true)
            {
                PokerWindow window = new PokerWindow(values, suits);
                window.Visibility = Visibility.Visible;
            }
        }
    }
}
