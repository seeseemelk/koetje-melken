﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace KaartSpelen
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class KoetjeMelkenWindow : Window
	{
        private Deck computerHand = new Deck();
        private Deck computerTable = new Deck();
        private Deck playerHand = new Deck();
        private Deck playerTable = new Deck();
        private BitmapImage cardback;
        private DispatcherTimer timer = new DispatcherTimer();
        private int playerNrCardsDrawn = 0;
        private int computerNrCardsDrawn = 0;
        private int computerScore = 0;
        private int playerScore = 0;
        private string[] values;
        private string[] suits;
      

		public KoetjeMelkenWindow(string[] values, string[] suits)
		{
			InitializeComponent();
            this.suits = suits;
            this.values = values;
            cardback = new BitmapImage();
            cardback.BeginInit();
            cardback.UriSource = new Uri("resources/cardback.png", UriKind.RelativeOrAbsolute);
            cardback.EndInit();
            timer.Interval = TimeSpan.FromSeconds(2);
            timer.Tick += OnTick;
        }

        private void OnTick(Object sender, EventArgs e)
        {
            timer.IsEnabled = false;
            shuffledLabel.Visibility = Visibility.Hidden;
            if (!playerHand.IsEmpty())
            {
                dealButton.IsEnabled = true;
            }
            computerTable.Clear();
            if (!computerHand.IsEmpty())
            {
                Card computerCard = computerHand.TakeCard();
                computerTable.AddCard(computerCard);
                computerCardNameLabel.Content = computerCard.ToString();
                computerNrCardsDrawn++;
                computerCardNrLabel.Content = $"Card #{computerNrCardsDrawn}";
            }
            playerTable.Clear();
            playerWinLabel.Content = "";
            computerWinLabel.Content = "";
            Draw();
        }

        private void Draw(Canvas canvas, Deck deck)
        {
            canvas.Children.Clear();
            if (!deck.IsEmpty())
            {
                Image card = new Image();
                card.Source = deck.GetTopCard().GetImage();
				card.Width = canvas.Width;
                card.MouseDown += dealButton_Click;
                canvas.Children.Add(card);
            }  
        }

        private void DrawReverse(Canvas canvas, Deck deck)
        {
            canvas.Children.Clear();
            if (!deck.IsEmpty())
            {
                Image card = new Image();
                card.Source = cardback;
                card.Width = canvas.Width;
                card.MouseDown += dealButton_Click;
                canvas.Children.Add(card);
            }
        }

        private void Draw()
        {
            Draw(computerTableCanvas, computerTable);
            Draw(playerTableCanvas, playerTable);
            DrawReverse(computerHandCanvas, computerHand);
            DrawReverse(playerHandCanvas, playerHand);
        }

        private void shuffleButton_Click(object sender, RoutedEventArgs e)
        {
            dealButton.IsEnabled = false;
            computerScore = 0;
            playerScore = 0;
            computerNrCardsDrawn = 0;
            playerNrCardsDrawn = 0;
            playerCardNrLabel.Content = "";
            computerCardNrLabel.Content = "";
            computerCardNameLabel.Content = "";
            playerCardNameLabel.Content = "";
            statusTextBlock.Text = "";

            computerHand.Clear();
            computerTable.Clear();
            playerHand.Clear();
            playerTable.Clear();

            computerHand.Fill(values, suits);
            computerHand.Shuffle();
            playerHand.Fill(values, suits);
            playerHand.Shuffle();

            shuffledLabel.Visibility = Visibility.Visible;
            timer.IsEnabled = true;
            Draw();
        }

        private void dealButton_Click(object sender, RoutedEventArgs e)
        {
            if (!dealButton.IsEnabled)
            {
                return;
            }
            if (!playerHand.IsEmpty())
            {
                Card playerCard = playerHand.TakeCard();
                playerTable.AddCard(playerCard);
                playerCardNameLabel.Content = playerCard.ToString();
                playerNrCardsDrawn++;
                playerCardNrLabel.Content = $"Card #{playerNrCardsDrawn}";
                if (computerTable.GetTopCard().Score < playerCard.Score)
                {
                    playerWinLabel.Content = "You win";
                    playerScore++;
                }
                else if(playerCard.Score < computerTable.GetTopCard().Score)
                {
                    computerWinLabel.Content = "Computer wins";
                    computerScore++;
                }
                else
                {
                    playerWinLabel.Content = "Equal";
                    computerWinLabel.Content = "Equal";
                }
            }
            if(playerHand.IsEmpty())
            {
                computerCardNameLabel.Content = "NO MORE CARDS TO DEAL";
                playerCardNameLabel.Content = "NO MORE CARDS TO DEAL";
                computerCardNrLabel.Content = "Click shuffle cards to continue";
                playerCardNrLabel.Content = "Click shuffle cards to continue";

                if(computerScore > playerScore)
                {
                    statusTextBlock.Text = $"Sorry, you lost. The computer won {computerScore} times, you won {playerScore} times.";
                }
                else if(computerScore < playerScore)
                {
                    statusTextBlock.Text = $"Congratulations!! You are the winner!! The computer won {computerScore} times, you won {playerScore} times.";
                }
                else
                {
                    statusTextBlock.Text = $"No winner, you have the same result as the computer: you won {playerScore} times.";
                }
            }

            dealButton.IsEnabled = false;
            timer.IsEnabled = true;
            Draw();



        }
        
        private void exitMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
